const {google} = require('googleapis');

const youtube = google.youtube({
    version: 'v3',
    auth: process.env.YOUTUBE_API_KEY
});

function getLast24h(list) {

}

class Channel {
    constructor(id, filter) {
      this.id = id;
      this.filter = filter;
    }
}

class List {
    constructor(id, filter) {
        this.id = id;
        this.filter = filter;
    }
}

class Video {
    constructor(id) {
        this.id = id;
    }
}

exports.module = {
    Channel,
    List,
    Video
}