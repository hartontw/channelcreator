const fs = require('fs');
const path = require('path');
const ffmpeg = require('../services/ffmpeg');

module.exports = class Source {
    constructor(filePath, info) {
        this._file = filePath;
        this._info = info;
    }

    init() {
        return new Promise( (resolve, reject) => {
            this.download()
            .then(time => {
                this.load()
                .then(resolve)
                .catch(reject);
            })
            .catch(reject);
        });
    }

    download() {
        return new Promise( (resolve, reject) => {
            if (fs.existsSync(this.file)) {
                resolve();
            }
            else {
                const start = process.hrtime();
                this.downloadRaw()
                .then( () => {
                    const time = process.hrtime(start)[0];
                    console.log(`Downloaded in ${time} seconds.`);
                    resolve(time);
                })
                .catch(reject);
            }
        });
    }

    downloadRaw() {
        return new Promise( resolve => {
            resolve();
        });
    }

    load() {
        return new Promise( (resolve, reject) => {
            ffmpeg.getInfo(this.file)
                .then(info => {
                    if (this.info) {
                        if (this.info.video) {
                            for(let i=0; i < info.video.length; i++) {
                                info.video[i].title = info.video[i].title || this.info.video[i].title;
                                info.video[i].language = info.video[i].language || this.info.video[i].language;
                            }
                        }
                        if (this.info.audio) {
                            for(let i=0; i < info.audio.length; i++) {
                                info.audio[i].title = info.audio[i].title || this.info.audio[i].title;
                                info.audio[i].language = info.audio[i].language || this.info.audio[i].language;
                            }
                        }
                        if (this.info.subtitle) {
                            for(let i=0; i < info.subtitle.length; i++) {
                                info.subtitle[i].title = info.subtitle[i].title || this.info.subtitle[i].title;
                                info.subtitle[i].language = info.subtitle[i].language || this.info.subtitle[i].language;
                            }
                        }
                    }
                    this._info = info;
                    resolve(info);
                })
                .catch(err => {
                    reject(err && err !== '' ? err : 'Not valid video format');
                });
        });
    }

    isValidFormat(filePath) {
        return new Promise( (resolve, reject) => {
            const extension = path.extname(filePath).toLowerCase();
            ffmpeg.getFormats()
            .then(formats => {
                const filter = formats.filter(f => f.extension === extension);
                resolve(filter.length > 0);
            })
            .catch(reject);
        })
    }

    remove() {
        if (fs.existsSync(this.file)) {
            fs.unlinkSync(this.file);
        }
    }

    get file() {
        return this._file;
    }

    get info() {
        return this._info;
    }

    get ready() {
        return this.info && fs.existsSync(this.file);
    }    
}