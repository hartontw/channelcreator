const fs = require('fs');
const path = require('path');
const https = require('https');
const Source = require('./source');

module.exports = class HTTPS extends Source {
    constructor(url, downloadDir, info) { 
        super(undefined, info);
        this.url = url;
        this.downloadDir = downloadDir;
    }

    downloadRaw() {
        return new Promise( (resolve, reject) => {
            const fileName = path.basename(this.url);
            this._file = path.join(this.downloadDir, fileName);
            const file = fs.createWriteStream(this.file);
            https.get(this.url, response => {
                response.pipe(file);
                file.on('finish', () => {
                    resolve();
                });
                file.on('error', (err) => {
                    reject(err);
                });
            });
        });
    }
}