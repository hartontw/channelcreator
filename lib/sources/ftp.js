const fs = require('fs');
const path = require('path');
const ftp = require("basic-ftp")
const Source = require('./source');

module.exports = class HTTPS extends Source {
    constructor(filePath, downloadDir, ftpConfig, info) { 
        super(undefined, info);
        this.filePath = filePath;
        this.downloadDir = downloadDir;
        this.ftpConfig = ftpConfig || {
            host : process.env.FTP_HOST,
            user : process.env.FTP_USER,
            password : process.env.FTP_PASSWORD,
            secure : process.env.FTP_SECURE,
        };
    }

    downloadRaw() {
        return new Promise( async (resolve, reject) => {
            const client = new ftp.Client()
            //client.ftp.verbose = true
            try {
                await client.access(ftpConfig);
                //console.log(await client.list())
                //await client.uploadFrom("README.md", "README_FTP.md")
                //await client.downloadTo("README_COPY.md", "README_FTP.md")
            }
            catch(err) {
                reject(err);
            }
            client.close()
        });
    }
}