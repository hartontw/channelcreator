const Transmission = require('transmission')
const fs = require('fs');
const path = require('path');
const Source = require('./source');

module.exports = class Torrent extends Source {

    constructor(torrent, downloadDir, transmissionConfig, info) { 
        super(undefined, info);
        this.torrent = torrent;
        this.downloadDir = downloadDir;

        transmissionConfig = transmissionConfig || {
            port : process.env.TRANSMISSION_PORT,
            host : process.env.TRANSMISSION_IP,     
        };

        this.transmission = new Transmission(transmissionConfig);
    }

    downloadRaw(timeout = 10000) {
        return new Promise( (resolve, reject) => {
            this.transmission.addUrl(this.torrent, {
                'download-dir' : this.downloadDir
            }, (err, result) => {
                if (!err) {
                    this._result = result;
                    this._file = path.join(this.downloadDir, result.name);                    
                    const check = () => {
                        this.transmission.get(result.id, async (err, result) => {
                            if (err) {
                                reject(err);
                            }                       
                            else if(result.torrents.length > 0) {
                                this._result = result.torrents[0];
                                if (this.isDownloaded) {                              
                                    if (fs.lstatSync(this._file).isDirectory()) {
                                        this._isDirectory = true;
                                        const files = fs.readdirSync(this._file);
                                        for(let file of files) {
                                            try {
                                                const valid = await isValidFormat(file);
                                                if (valid) {
                                                    this._file = file;
                                                    break;
                                                }
                                            }
                                            catch(err) {
                                                reject(err);
                                                return;
                                            }
                                        }
                                    }                                    
                                    resolve();
                                }
                                else setTimeout( () => {check();}, timeout);
                            }
                            else {
                                reject('Not torrent found');
                            }
                        });
                    };
                    setTimeout( () => {check();}, timeout);
                }
                else reject(err);
            });
        });
    }

    remove() {
        if (this._isDirectory) {
            const dirname = path.dirname(this.file);
            fs.unlinkSync(dirname);
        }
        else super.remove();
    }

    get status() {
        return this.transmission.statusArray[this._result.status];
    }

    get isDownloaded() {
        return this._result.percentDone >= 1;
    }

    get ready() {
        this.isDownloaded() && super.ready;
    }
}