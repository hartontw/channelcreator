const fs = require('fs');
const ffmpeg = require('./services/ffmpeg');
const countriesAPI = require('./services/countries');
const epg = require('./epg');
const MovieDB = require('./services/moviedb');
const moviedb = new MovieDB();

class Programme {
    constructor(media, title, airDate, subtitle, description, genres, certification, countries, image, rating, credits) {        
        this.media = media;
        this.title = title;
        this.airDate = airDate && new Date(airDate);
        this.subtitle = subtitle;
        this.description = description;
        this.genres = genres;
        this.certification = certification;
        this.countries = countries;        
        this.image = image;
        this.rating = rating;
        this.credits = credits;
    }

    toEPG() {
        return epg.getProgramme(this);
    }

    static Create(filePath, title, airDate, subtitle, description, genres, certification, countries, image, rating, credits) {
        return new Promise( (resolve, reject) => {
            if (fs.existsSync(filePath)) {
                ffmpeg.getInfo(filePath)
                .then(async (info) => {
                    if (process.env.MOVIEDB_LANGUAGE !== 'en') {
                        try {
                            countries = await countriesAPI.fix(process.env.MOVIEDB_LANGUAGE, countries);
                        }
                        catch(err) {}
                    }
                    resolve(new Programme(info, title, airDate, subtitle, description, genres, certification, countries, image, rating, credits));
                })
                .catch(reject);
            }
            else reject(`${filePath} not found`);
        });
    }
}

class Movie extends Programme {
    constructor(media, title, releaseDate, original_title, description, genres, certification, countries, image, rating, credits) {
        super(media, title, releaseDate, original_title, description, genres, certification, countries, image, rating, credits);
    }

    toEPG() {
        return epg.getMovie(this);
    }

    static Create(filePath, name, year) {
        return new Promise( (resolve, reject) => {
            if (fs.existsSync(filePath)) {
                ffmpeg.getInfo(filePath)
                .then(info => {
                    moviedb.getMovie(name, year)
                    .then(async (movie) => {
                        let production_countries = movie.production_countries;
                        if (process.env.MOVIEDB_LANGUAGE !== 'en') {
                            try {
                                production_countries = await countriesAPI.fix(process.env.MOVIEDB_LANGUAGE, movie.production_countries);
                            }
                            catch(err) {}
                        }

                        resolve(new Movie(
                            info, 
                            movie.title,
                            movie.release_date,
                            movie.original_title,
                            movie.overview,
                            movie.genres && movie.genres.map(g => g.name),
                            movie.certification,
                            production_countries,                            
                            movie.poster_path,
                            movie.vote_average && movie.vote_count > 0 && movie.vote_average / 10,
                            MovieDB.formatMovieCredits(movie),
                        ));
                    })
                    .catch(reject);
                })
                .catch(reject);
            }
            else reject(`${filePath} not found`);
        });
    }
}

class Show extends Programme {
    constructor(media, showName, airDate, season, episode, episodeName, description, genres, certification, countries, image, rating, credits) {
        super(media, showName, airDate, episodeName, description, genres, certification, countries, image, rating, credits);
        this.season = season;
        this.episode = episode;
    }

    toEPG() {
        return epg.getShow(this);
    }

    static Create(filePath, name, season_number, episode_number, year) {
        return new Promise( (resolve, reject) => {
            if (fs.existsSync(filePath)) {
                ffmpeg.getInfo(filePath)
                .then(info => {
                    moviedb.getEpisode(name, season_number, episode_number, year)
                    .then(async (episode) => {                       
                        let origin_country = episode.show.origin_country;
                        if (process.env.MOVIEDB_LANGUAGE !== 'en') {
                            try {
                                origin_country = await countriesAPI.fix(process.env.MOVIEDB_LANGUAGE, episode.show.origin_country);
                            }
                            catch(err) {}
                        }

                        resolve(new Show(
                            info, 
                            episode.show.name,
                            episode.air_date,
                            season_number,
                            episode_number,
                            episode.name,
                            episode.overview || episode.show.overview,
                            episode.show.genres && episode.show.genres.map(g => g.name),
                            episode.show.certification,
                            origin_country,                            
                            episode.still_path || episode.show.poster_path,
                            episode.vote_average && episode.vote_count > 0 && episode.vote_average / 10,
                            MovieDB.formatEpisodeCredits(episode),
                        ));
                    })
                    .catch(reject);
                })
                .catch(reject);
            }
            else reject(`${filePath} not found`);
        });
    }
}

function Create(filePath) {
    return new Promise( (resolve, reject) => {
        let fileName = filePath.match(/(([^\/]+)\.)/)[2];
        const showInfo = fileName.match(/S(\d+)[_ ]*E(\d+)/);
        let year = showInfo && fileName.match(/\((\d{4})\)/); year = year && year[1];
        let name = year ? fileName.replace(/\(\d{4}\)/, '') : fileName;
        
        if (showInfo) {
            Show.Create(filePath, name.replace(/[_ ]*S\d+[_ ]*E\d+.+/, ''), showInfo[1], showInfo[2], year)
            .then(resolve)
            .catch( () => {
                Programme.Create(filePath, name, year)
                .then(resolve)
                .catch(reject);
            });
        }
        else
        {
            Movie.Create(filePath, name, year)
            .then(resolve)
            .catch( () => {
                Programme.Create(filePath, name, year)
                .then(resolve)
                .catch(reject);
            });
        }
    });
}

module.exports = {
    Programme,
    Movie,
    Show,
    Create,
};