const fs = require('fs');
const path = require('path');
const environment = fs.existsSync('./config/environment.json') ? JSON.parse(fs.readFileSync('./config/environment.json', 'utf8')) : {};
const epgTranslations = fs.existsSync('./config/epgTranslations.json') ? JSON.parse(fs.readFileSync('./config/epgTranslations.json', 'utf8')) : {};
const kodiGenres = fs.existsSync('./config/kodiGenres.json') && JSON.parse(fs.readFileSync('./config/kodiGenres.json', 'utf8'));

function getContentPath(p) {
    if (!p) {
        return path.join(__dirname, '../content');
    }
    const parse = path.parse(p);
    if (parse.root === '') {
        return path.join(__dirname, `../${p.replace(/^[\.\/]*/g, '')}`);
    }
    return p;
}

process.env.IP = process.env.IP || environment.ip || '127.0.0.1';
process.env.PORT = process.env.PORT || environment.port || 8080;
process.env.CONTENT_PATH = process.env.CONTENT_PATH || getContentPath(environment.content_path);
process.env.EPG_DOMAIN = process.env.EPG_DOMAIN || environment.epg && environment.epg.domain || 'channelCreator';
process.env.EPG_LANGUAGE = process.env.EPG_LANGUAGE || environment.epg && environment.epg.language || 'en';
process.env.EPG_CERTIFICATION = process.env.EPG_CERTIFICATION || environment.epg && environment.epg.certification || 'US';
process.env.EPG_KODI = process.env.EPG_KODI || environment.epg && environment.epg.kodi;
process.env.MOVIEDB_API_KEY = process.env.MOVIEDB_API_KEY || environment.moviedb && environment.moviedb.api_key;
process.env.MOVIEDB_LANGUAGE = process.env.MOVIEDB_LANGUAGE || environment.moviedb && environment.moviedb.language;
process.env.MOVIEDB_REGION = process.env.MOVIEDB_REGION || environment.moviedb && environment.moviedb.region;
process.env.MOVIEDB_CERTIFICATION_COUNTRY = process.env.MOVIEDB_CERTIFICATION_COUNTRY || environment.moviedb && environment.moviedb.certification_country;
process.env.YOUTUBE_API_KEY = process.env.YOUTUBE_API_KEY || environment.youtube && environment.youtube.api_key;
process.env.TRANSMISSION_IP = process.env.TRANSMISSION_IP || environment.transmission && environment.transmission.ip || '127.0.0.1';
process.env.TRANSMISSION_PORT = process.env.TRANSMISSION_PORT || environment.transmission && environment.transmission.port || 9091;

global._getPath = p => {
    const _path = path.join(process.env.CONTENT_PATH, p);
    const info = path.parse(_path);
    const dir = info.ext !== '' ? info.dir : _path;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, { recursive: true });
    }
    return _path;
};

global._timeToSeconds = time => {
    if (typeof(time) === 'string') {
        time = time.split(':');
        return time[0]*3600 + time[1]*60 + (time.length > 2 ? time[2]*1 : 0);
    }
    return time;
};

global._secondsToTime = seconds => {
    let hours = Math.floor(seconds/3600) % 24;
    let minutes = Math.floor(seconds/60) % 60;
    seconds = Math.floor(seconds % 60);
    return `${hours}:${minutes}:${seconds}`;
};

global._now = () => {
    const date = new Date();
    date.setMinutes(date.getMinutes()-date.getTimezoneOffset())
    return date;
};

global._tab = amount => {
    let space = '';
    while(amount-- > 0) {
        space += ' ';
    }
    return space;
}

global._epgTranslation = field => {
    return epgTranslations[field] || field;
}

global._getKodiGenre = (genreString, language, english = false) => {
    if (!kodiGenres) return genreString;

    const from = genreString.split('/');
    let selected = -1;
    let max = 0;
    for(let i in kodiGenres[language]) {        
        let to = kodiGenres[language][i].split('/');

        let matchs = 0;
        for(let toGenre of to) {
            toGenre = toGenre.toLowerCase().trim();
            for (let fromGenre of from) {
                fromGenre = fromGenre.toLowerCase().trim();
                if (toGenre.includes(fromGenre) || fromGenre.includes(toGenre)) {
                    matchs++;
                    break;
                }
            }
        }

        if (matchs > max) {
            max = matchs;
            selected = i;
        }
    }
    if (selected < 0)
        return genreString;

    return english ? kodiGenres['en'][selected] : kodiGenres[language][selected];
}

if (!Date.prototype.toEPGString) {
    (function() {
  
      function pad(number) {
        if (number < 10) {
          return '0' + number;
        }
        return number;
      }

      function offset(number) {
          number *= -1;
          const sign = number < 0 ? '-' : '+';
          number = Math.abs(number);
          const hour = pad(Math.floor(number/60));
          const min = pad(number % 60);
          return sign + hour + min;
      }
  
      Date.prototype.toEPGString = function() {
        return this.getUTCFullYear() +
          pad(this.getUTCMonth() + 1) +
          pad(this.getUTCDate()) +
          pad(this.getUTCHours()) +
          pad(this.getUTCMinutes()) +
          pad(this.getUTCSeconds()) +
          ' ' + offset(this.getTimezoneOffset());
      };
  
    })();
  }