const fs = require('fs');
const path = require('path');

function getConfigPath() {
    return _getPath('/config/channels');
}

function getFilePath(channelName) {
    return `${getConfigPath()}/${channelName}.json`;
}

class GuideItem {
    constructor(date, programme) {
        this.start = date;
        this.stop = new Date(date.getTime() + programme.media.duration);
        this.duration = programme.media.duration;
        this.programme = programme;
    }

    toEPG(channel) {          
        let epg = `${_tab(2)}<programme start="${this.start.toEPGString()}" stop="${this.stop.toEPGString()}" channel="${channel.id}.${process.env.EPG_DOMAIN}">\n`;
        epg += this.programme.toEPG();
        epg += `${_tab(2)}</programme>\n`;
        return epg;
    }
}

class Guide {
    constructor(items = []) {
        this._items = items;
    }

    get items() {
        let copy = [];
        for(let p of this._items) {
            copy.push(p);
        }
        return copy;
    }

    get last() {
        return this._items.length > 0 && this._items[this._items.length-1];
    }

    get current() {
        const now = _now();
        for(let item of this._items) {
            if (item.start >= now && item.stop >= now) {
                return item;
            }
        }
    }

    removeOld() {
        const now = _now();
        let i = 0;
        while(i < this._items.length) {
            if (this._items[i].stop < now) {
                this._items.splice(i, 1);
            }
            else i++;
        }
    }

    addProgramme(programme, date) {
        return new Promise( (resolve, reject) => {
            const now = _now();            

            if (date) {
                if (date < now) {
                    reject('Date cant be before now');
                    return;
                }

                const guideItem = new GuideItem(date, programme);

                for(let i=0; i<this._items.length; i++) {
                    if (guideItem.start < this._items[i].start) {
                        if (guideItem.stop <= this._items[i].stop) {
                            this._items.splice(i, splice, guideItem);
                            resolve(guideItem);
                        }
                        else {
                            reject('Program overlaping');
                        }
                        return;
                    }
                }

                this._items.push(guideItem);
                resolve(guideItem);
            }
            else {
                const guideItem = 
                    this._items.length > 0 && this.last.stop >= now 
                    ? new GuideItem(new Date(this.last.stop), programme)
                    : new GuideItem(now, programme);

                this._items.push(guideItem);
                resolve(guideItem);
            }
        });
    }

    shiftProgramme(programme, date) {
        return new Promise( (resolve, reject) => {
            const now = _now(); 
        
            if (date) {
                if (date < now) {
                    reject('Date cant be before now');
                    return;
                }

                const guiteItem = new GuideItem(date, programme);

                let index = -1;
                for(let i in this._items) {
                    if (guiteItem.start < this._items[i].start) {                
                        index = i;
                        break;
                    }
                }

                if (index >= 0) {
                    this._items.splice(index, 0, guiteItem);

                    let stop = guiteItem.stop.getTime();
                    for(let i=index+1; i < this._items.length; i++) {
                        let ms = this._items[i].start.getTime();
                        let diff = stop - ms;
                        if (diff > 0) {
                            ms += dif;
                            this._items[i].start = new Date(ms);
                            stop = ms + this._items[i].duration;
                            this._items[i].stop = new Date(stop);
                        }
                        else break;
                    }

                    resolve(guiteItem);
                }
                else {
                    this._items.push(guiteItem);
                    resolve(guiteItem);
                }
            }
            else {
                const guideItem = 
                    this._items.length > 0 && this.last.stop >= now 
                    ? new GuideItem(new Date(this.last.stop), programme)
                    : new GuideItem(now, programme);

                this._items.push(guideItem);
                resolve(guideItem);
            }
        });
    }

    fillGap(programme) {
        const now = _now();

        let guideItem;
  
        for (let i=0; i < this._items.length; i++) {
            const stop = this._items[i].stop;
            if (stop >= now) {
                if (i === this._items.length - 1 || stop.getTime() + programme.media.duration <= this._items[i+1].start.getTime()) {                    
                    guideItem = new GuideItem(new Date(stop), programme);
                    this._items.splice(i+1, 0, guideItem);
                    return guideItem;
                }
            }
        }

        guideItem = new GuideItem(now, programme);
        this._items.push(guideItem);
        return guideItem;
    }

    toEPG(channel) {
        let epg = '';
        for(let item of this._items) {
            epg += item.toEPG(channel);
        }
        return epg;
    }
}

class Channel {
    constructor(id, name, group, logo, testcard, interlude, missing, guide) {
        this.id = id;
        this.name = name || id;
        this.group = group;
        this.logo = logo;
        this.testcard = testcard;
        this.interlude = interlude;
        this.missing = missing;
        this.guide = guide || new Guide();
    }

    write() {
        const filePath = getFilePath(this.id);
        const ch = JSON.stringify(this);
        fs.writeFileSync(filePath, ch);
    }

    toEPG() {
        let epg = '';
        epg += `${_tab(2)}<channel id="${this.id}.${process.env.EPG_DOMAIN}">\n`;
        epg += `${_tab(4)}<display-name>${this.name}</display-name>\n`;
        if (this.logo) {
            epg += `${_tab(4)}<icon src="${this.logo}" />\n`;
        }
        epg += `${_tab(2)}</channel>\n`;
        return epg;
    }

    static read(name) {
        const filePath = getFilePath(name);
        const ch = JSON.parse(fs.readFileSync(filePath, 'utf8'));
        return new Channel(ch.id, ch.name, ch.group, ch.logo, ch.testcard, ch.interlude, ch.missing, ch.guide);
    }    

    static readAll() {
        const configPath = getConfigPath();
        const files = fs.readdirSync(configPath);
        const channels = [];
        files.forEach(file => {
            if (path.extname(file) === '.json') {
                const filePath = `${configPath}/${file}`;
                const ch = JSON.parse(fs.readFileSync(filePath, 'utf8'));
                channels.push(new Channel(ch.id, ch.name, ch.group, ch.logo, ch.testcard, ch.interlude, ch.missing, ch.guide));
            }
        });
        return channels;
    }

    static toEPG(channels) {
        let epg = '';
        epg += `<?xml version="1.0" encoding="UTF-8"?>\n`;
        epg += `<tv generator-info-name="channelCreator" generator-info-url="https://github.com/hartontw/channelCreator">\n`;
        for(let channel of channels) {
            epg += channel.toEPG();
        }
        for(let channel of channels) {
            epg += channel.guide.toEPG(channel);
        }
        epg += `</tv>`;
        return epg;
    }
}

module.exports = Channel;