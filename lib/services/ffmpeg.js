const fs = require('fs');
const path = require('path');
const exec = require('child_process').exec;

function sendCommand(command)
{
    return new Promise( (resolve, reject) => {
        exec(command, (error, stdout, stderr) => {
            if (!error)
                resolve(stdout);
            else 
                reject(stderr);
        });
    });
}

function getFormats() {
    return new Promise( (resolve, reject) => {
        sendCommand(`ffmpeg -formats`)
        .then(stdout => {
            const res = [];
            const rows = stdout.split('\n');
            for(let i = 4; i < rows.length-1; i++) {
                const row = rows[i].match(/[ \t]+(\S)+[ \t]+([^ \t]+)[ \t]+([^\r\n]+)/);
                res.push({
                    name: row[3],
                    extension: row[2],
                    muxing: row[1].includes('M'),
                    demuxing: row[1].includes('D')
                });
            }
            resolve(res);
        })
        .catch(reject);
    });
}

function getRawInfo(filePath) {
    return new Promise( (resolve, reject) => {
        sendCommand(`ffprobe -v quiet -print_format json -show_format -show_streams "${filePath}"`)
        .then(stdout => {
            resolve(JSON.parse(stdout));
        })
        .catch(reject);
    });
}

function getInfo(filePath) {
    return new Promise( (resolve, reject) => {
        getRawInfo(filePath)
        .then(info => {
            const duration = Math.floor(info.format.duration * 1000);
            const size = info.format.size;
            const creation_time = info.format.creation_time;
            const video = [], audio = [], subtitle = [];
            for(let i=0; i<info.streams.length; i++) {
                const stream = {
                    title: info.streams[i].tags && info.streams[i].tags.title,
                    language: info.streams[i].tags && info.streams[i].tags.language,
                    codec_name: info.streams[i].codec_name,
                    codec_long_name: info.streams[i].codec_long_name
                }
                switch(info.streams[i].codec_type){
                    case 'video':
                        stream.profile = info.streams[i].profile;
                        stream.width = info.streams[i].width;
                        stream.height = info.streams[i].height;
                        stream.sample_aspect_ratio = info.streams[i].sample_aspect_ratio;
                        stream.display_aspect_ratio = info.streams[i].display_aspect_ratio;
                        stream.pix_fmt = info.streams[i].pix_fmt;
                        stream.bit_rate = info.streams[i].bit_rate;
                        video.push(stream);
                        break;

                    case 'audio':
                        stream.channels = info.streams[i].channels;
                        stream.channel_layout = info.streams[i].channel_layout;
                        stream.bit_rate = info.streams[i].bit_rate;
                        audio.push(stream);
                        break;

                    case 'subtitle':
                        subtitle.push(stream);
                        break;
                }
            }
            resolve({
                filePath,
                duration,
                size,
                creation_time,
                video,
                audio,
                subtitle
            });
        })
        .catch(reject);
    });
}

function getDuration(filePath) {
    return new Promise( (resolve, reject) => {
        sendCommand(`ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 ${filePath}`)
        .then(stdout => {
            const duration = Math.floor(stdout.replace('\r\n', '') * 1000);
            resolve(duration);
        })
        .catch(reject);
    });
}

function convert(filePath, destinationPath, remove) {
    return new Promise( (resolve, reject) => {
        if (!fs.existsSync(filePath)) {
            reject(`${filePath} not found`);
        }
        else if (fs.existsSync(destinationPath)) {
            reject(`${destinationPath} already exists`);
        }
        else {
            sendCommand(`ffmpeg -i "${filePath}" -map 0 -c copy -c:s mov_text "${destinationPath}"`)
            .then(stdout => {
                if (remove) {
                    fs.unlinkSync(filePath);
                }
                resolve(stdout);
            })
            .catch(reject);
        }
    });
}

// Without subtitles
// TODO: Extract srt subs, convert to bitmap, insert
function split(filePath, destinationPath, listName, segmentTime, segmentFormat, remove) {
    return new Promise( (resolve, reject) => {
        if (!fs.existsSync(filePath)){
            reject(`${filePath} not found`);
        }
        else {
            sendCommand(`ffmpeg -i "${filePath}" -map 0 -c copy -sn -f segment -segment_time ${segmentTime} -segment_list "${destinationPath}/${listName}.m3u8" "${destinationPath}/${segmentFormat}.ts"`)
            .then(stdout => {
                if (remove) {
                    fs.unlinkSync(filePath);
                }
                resolve(stdout);
            })
            .catch(reject);
        }
    });
}

function extractTextSubs(filePath, destinationPath) {
    return new Promise( (resolve, reject) => {
        getInfo(filePath)
        .then( async info => {
            if (info.subtitle.length > 0) {
                for(let i in info.subtitle) {
                    try {
                        await sendCommand(`ffmpeg -i "${filePath}" -map 0:s:${i} "${destinationPath}/${info.subtitle[i].title || i}.srt"`);
                    }
                    catch(err) {
                        reject(err);
                        return;
                    }
                }
                resolve(info.subtitle);
            }
            else reject('Not subtitles found.');
        })
        .catch(reject);
    });
}

function subtitlesToImages(filePath) {
    return new Promise( async (resolve, reject) => {
        const name = path.basename(filePath, path.extname(filePath));
        const subsPath = `${path.dirname(filePath)}/${name}`;
        if (!fs.existsSync(subsPath)) fs.mkdirSync(subsPath);
        const subs = fs.readFileSync(filePath, 'utf8');
        const times = subs.match(/^\d{2}:\d{2}:\d{2}/gm);
        const pad = num => String(num).padStart(2, '0')
        for(let i in times) {
            const split = times[i].split(':');
            let h = split[0] * 1;
            let m = split[1] * 1;
            let s = split[2] * 1 + 1;
            if (s > 59) {
                s = 0;
                m += 1;
                if (m > 59) {
                    m = 0;
                    h += 1;
                }
            }
            times[i] = pad(h) + ':' + pad(m) + ':' + pad(s);
            try {
                const command = `ffmpeg -ss ${times[i]} -f lavfi -i "color=color=white@0.0:size=200x200,format=rgba,subtitles=Castellano.srt':alpha=1" -frames:v 1 "Subs/${i+1}.png"`;
                await sendCommand(command);
            }
            catch(err) {
                reject(err);
                return;
            }
        }
        resolve();
    });
}

module.exports = {
    getFormats,
    getRawInfo,
    getInfo,
    getDuration,
    convert,
    split,
    extractTextSubs,
    subtitlesToImages
}

//CopyAll mkv-->mp4 | mp4-->mkv
//ffmpeg -i Video.mkv -map 0 -c copy -c:s mov_text Video.mp4

//Split in transport stream without subs
//ffmpeg -i Video.mkv -map 0 -c copy -sn -f segment -segment_time 500 -segment_list Video.m3u8 Video%05d.ts
//ffmpeg -i Video.mkv -map 0 -vcodec libx264 -acodec aac -sn -f segment -segment_time 500 -segment_list Video.m3u8 Video%05d.ts

//Extract 2 first subs 
//ffmpeg -i Video.mkv -map 0:s:0 -f webvtt VideoEnglish.webvtt -map 0:s:1 -f webvtt VideoSpanish.webvtt
//ffmpeg -i Video.mkv -map 0:s:0 -f srt VideoEnglish.srt -map 0:s:1 -f webvtt VideoSpanish.srt

//Split first sub
//ffmpeg -i VideoEnglish.webvtt -f segment -segment_time 300 -segment_list_size 0 -segment_list VideoEnglish.m3u8 -segment_format webvtt -scodec copy VideoEnglish-%d.webvtt

//Extract dvdsubs
//mencoder Video.ts -ifo movie.ifo -vobsubout subtitles -vobsuboutindex 0 -vobsuboutid it -sid 1 -nosound -ovc copy

//Copy with subs
//mencoder video.avi -oac copy -ovc lavc -sub subtitle.srt -subfont-text-scale 3 -o output_video.avi