const https = require('https');
const API_URL = 'https://restcountries.eu/rest/v2';

function getCountry(iso_3166_1) {
    return new Promise( (resolve, reject) => {
        const url = `${API_URL}/alpha/${iso_3166_1}`
        https.get(url, res => {
            let body = '';
        
            res.on('data', chunk => {
                body += chunk;
            });
        
            res.on('end', () => {
                const country = JSON.parse(body);
                if (country.name) {
                    resolve(country);
                }                
                else {
                    reject(country.message);
                }
            });

        }).on('error', e => {
              reject(e);
        });
    });
}

function getCountryName(iso_3166_1, language) {
    return new Promise( (resolve, reject) => {
        getCountry(iso_3166_1)
        .then(country => {
            resolve(language && country.translations[language] || country.name);
        })
        .catch(reject);
    });
}

function getCountryByName(countryName) {
    return new Promise( (resolve, reject) => {
        const url = `${API_URL}/alpha/name/${countryName}`
        https.get(url, res => {
            let body = '';
        
            res.on('data', chunk => {
                body += chunk;
            });
        
            res.on('end', () => {
                const countries = JSON.parse(body);
                if (countries.status) {
                    reject(countries.message);
                }                
                else {
                    resolve(countries[0]);     
                }
            });

        }).on('error', e => {
              reject(e);
        });
    });
}

function fix(language, countries) {
    return new Promise( async (resolve, reject) => {
        const list = [];
        for(let country of countries) {
            let search = typeof(country) === 'object' ? country.iso_3166_1 || country.name : country;
            try {
                const res = await getCountry(search);
                list.push({
                    iso_3166_1: search,
                    name: res.translations[language] || res.name
                });
            }
            catch(err) {
                try {
                    const res = await getCountryByName(search);
                    list.push({
                        iso_3166_1: res.alpha2Code.toLowerCase(),
                        name: res.translations[language] || res.name
                    });
                }
                catch(err) {
                    reject(`Invalid country iso_3166_1 or name: ${search}`);
                    return;
                }
            }
        }
        resolve(list);
    });
}

module.exports = {
    getCountry,
    getCountryName,
    getCountryByName,
    fix
}