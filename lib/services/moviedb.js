const https = require('https');
const API_URL = 'https://api.themoviedb.org/3';
const IMG_URL = 'https://image.tmdb.org/t/p/w500';

function composeQuery(url, params)
{
    let query = url + '?';
    for(let key in params) {
        if (params[key])
            query += `${key}=${params[key]}&`;
    }
    return query.substr(0, query.length-1);
}

function execQuery(url) {
    return new Promise( (resolve, reject) => {
        https.get(url, res => {
            let body = '';
        
            res.on('data', chunk => {
                body += chunk;
            });
        
            res.on('end', () => {
                resolve(JSON.parse(body));
            });

        }).on('error', e => {
              reject(e);
        });
    });
}

function setImageUrl(object) {
    if (object) {
        Object.keys(object).forEach( key => {
            if (typeof(object[key]) === 'object') {
                setImageUrl(object[key]);
            }
            else if (typeof(object[key]) === 'string' && object[key].match(/\.(gif|jpg|jpeg|tiff|png)$/i)) {
                object[key] = `${IMG_URL}${object[key]}`;
            }
        });
    }
}

module.exports = class MovieDB {
    constructor(api_key, language, region, certification_country, include_adult) {
        this.api_key = api_key || process.env.MOVIEDB_API_KEY;
        this.language = language || process.env.MOVIEDB_LANGUAGE;
        this.region = region || process.env.MOVIEDB_REGION;
        this.certification_country = certification_country || process.env.MOVIEDB_CERTIFICATION_COUNTRY;
        this.include_adult = include_adult || true;
    }
    
    getMovie(name, year) {        
        return new Promise( (resolve, reject) => {
            MovieDB.searchMovie(this.api_key, this.language, name, this.include_adult, this.region, year)
            .then(result => {
                MovieDB.getMovieDetails(this.api_key, result.id, this.language, 'release_dates,credits')
                .then(movie => {
                    let cert = movie.release_dates && movie.release_dates.results;
                    cert = cert && cert.filter(c => c.iso_3166_1 === this.certification_country);
                    movie.certification = cert && cert[0] && cert[0].release_dates[0] && cert[0].release_dates[0].certification;
                    delete movie.release_dates;
                    setImageUrl(movie);
                    resolve(movie);
                })
                .catch( () => {
                    setImageUrl(result);
                    resolve(result);
                });
            })
            .catch(reject);
        });
    }

    getEpisode(showName, season_number, episode_number, first_air_date_year) {
        return new Promise( (resolve, reject) => {
            MovieDB.searchTVShow(this.api_key, this.language, showName, first_air_date_year)
            .then( result => {
                MovieDB.getTVShowDetails(this.api_key, result.id, this.language, 'content_ratings')
                .then(show => {
                    MovieDB.getTVEpisodeDetails(this.api_key, show.id, season_number, episode_number, this.language, 'credits')
                    .then(episode => {
                        episode.show = show;
                        let cert = show.content_ratings && show.content_ratings.results; 
                        cert = cert && cert.filter(c => c.iso_3166_1 === this.certification_country);
                        show.certification = cert && cert[0] && cert[0].rating;
                        delete show.content_ratings;
                        setImageUrl(episode);
                        resolve(episode);
                    })
                    .catch(reject);
                })
                .catch(reject);
            })
            .catch(reject);
        });
    }

    static getMovieDetails(api_key, movie_id, language, append_to_response) {
        const url = composeQuery(`${API_URL}/movie/${movie_id}`, {
            api_key, 
            language,
            append_to_response
        });
        return execQuery(url);
    }

    static getTVShowDetails(api_key, tv_id, language, append_to_response) {
        const url = composeQuery(`${API_URL}/tv/${tv_id}`, {
            api_key,
            language,
            append_to_response
        });
        return execQuery(url);
    }

    static getTVEpisodeDetails(api_key, tv_id, season_number, episode_number, language, append_to_response) {
        const url = composeQuery(`${API_URL}/tv/${tv_id}/season/${season_number}/episode/${episode_number}`, {
            api_key, 
            language,
            append_to_response
        });
        return execQuery(url);
    }

    static searchMovies(api_key, language, query, page, include_adult, region, year, primary_release_year) {
        const url = composeQuery(`${API_URL}/search/movie`, {
            api_key, 
            language, 
            query,
            page, 
            include_adult, 
            region, 
            year, 
            primary_release_year
        });
        return execQuery(url);
    }

    static searchMovie(api_key, language, query, include_adult, region, year, primary_release_year) {
        return new Promise( (resolve, reject) => {
            MovieDB.searchMovies(api_key, language, query, 1, include_adult, region, year, primary_release_year)
            .then(response => {
                if (response.results.length > 0)
                    resolve(response.results[0]);
                else 
                    reject('Nothing found');
            })
            .catch(reject);
        });
    }

    static searchTVShows(api_key, language, query, page, first_air_date_year) {
        const url = composeQuery(`${API_URL}/search/tv`, {            
            api_key, 
            language, 
            query,
            page, 
            first_air_date_year
        });
        return execQuery(url);
    }

    static searchTVShow(api_key, language, query, first_air_date_year) {
        return new Promise( (resolve, reject) => {
            MovieDB.searchTVShows(api_key, language, query, 1, first_air_date_year)
            .then(response => {
                if (response.results.length > 0)
                    resolve(response.results[0]);
                else
                    reject('Nothing found');
            })
            .catch(reject);
        });
    }

    static formatMovieCredits(movie) {
        const credits = {};
        if (movie.production_companies) {
            credits.companies = movie.production_companies.map(c => {
                return {
                    name: c.name,
                    country: c.origin_country
                };
            });
        }
        if (movie.credits.cast) {
            credits.cast = movie.credits.cast.map(c => {
                return {
                    name: c.name,
                    character: c.character,                
                };
            });
        }
        if (movie.credits.crew) {
            credits.crew = movie.credits.crew.map(c => {
                return {
                    name: c.name,
                    job: c.job,
                    department: c.department,                
                };
            });
        }
        return credits;
    }

    static formatEpisodeCredits(episode) {
        const credits = {};
        if (episode.show.created_by) {
            credits.created_by = episode.show.created_by.map(c => c.name);
        }
        if (episode.show.networks) {
            credits.networks = episode.show.networks.map(c => {
                return {
                    name: c.name,
                    country: c.origin_country
                };
            });
        }
        if (episode.show.production_companies) {
            credits.companies = episode.show.production_companies.map(c => {
                return {
                    name: c.name,
                    country: c.origin_country
                };
            });
        }
        if (episode.credits.cast) {
            credits.cast = episode.credits.cast.map(c => {
                return {
                    name: c.name,
                    character: c.character,                
                };
            });
        }
        if (episode.credits.crew) {
            credits.crew = episode.credits.crew.map(c => {
                return {
                    name: c.name,
                    job: c.job,  
                    department: c.department,                              
                };
            });
        }
        if (episode.credits.guest_stars) {
            credits.guest_stars = episode.credits.guest_stars.map(c => {
                return {
                    name: c.name,
                    character: c.character,                
                };
            });
        }
        return credits;
    }
};