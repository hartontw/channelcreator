const countries = require('./services/countries');

function getCountry(country, param) {
    return typeof(country) === 'object' ? country[param] : country;
}

function formatCountries(pc, amount = 2) {
    let length = Math.min(pc.length, amount);
    
    let pcs = '';
    for(let i=0; i<length; i++) {
        pcs += `${getCountry(pc[i], 'name')}, `;
    }
    return pcs.length > 0 && pcs.substr(0, pcs.length-2);
}

function getCrew(credits, job, category, amount = 2) {
    let result = '';
    if (credits && credits.crew) {
        credits = credits.crew.filter(c => c.job === job);
        for(let credit of credits) {
            if (amount-- === 0) break;
            result += `${_tab(6)}<${category}>${credit.name}</${category}>\n`;            
        }
    }
    return result;
}

function getCast(credits, amount = 5) {
    let result = '';
    if (credits && credits.cast) {
        for(let credit of credits.cast) {
            if (amount-- === 0) break;
            result += `${_tab(6)}<actor>${credit.name}</actor>\n`;            
        }
    }
    return result;
}

function getGuest(credits, amount = 5) {
    let result = '';
    if (credits && credits.guest) {
        for(let credit of credits.guest) {
            if (amount-- === 0) break;
            result += `${_tab(6)}<guest>${credit.name}</guest>\n`;            
        }
    }
    return result;
}

// DEFAULT
function getProgrammeDefault(programme) {
    const lang = process.env.EPG_LANGUAGE;
    let epg = '';
    epg += `${_tab(4)}<title lang="${lang}">${programme.title}</title>\n`;
    epg += programme.subtitle ? `${_tab(4)}<sub-title lang="${lang}">${programme.subtitle}</sub-title>` : '';
    epg += `${_tab(4)}<desc lang="${lang}">${programme.description || ''}</desc>\n`;
    if (programme.credits) {
        epg += `${_tab(4)}<credits>\n`;
        epg += getCrew(programme.credits, 'Director', 'director');
        epg += getCrew(programme.credits, 'Producer', 'producer');
        epg += getCrew(programme.credits, 'Screenplay', 'writer');
        epg += getCrew(programme.credits, 'Original Music Composer', 'composer');
        epg += getCrew(programme.credits, 'Presenter', 'presenter');
        epg += getCast(programme.credits);
        epg += getGuest(programme.credits);
        epg += `${_tab(4)}</credits>\n`;
    }
    epg += programme.airDate ? `${_tab(4)}<date>${programme.airDate.replace(/-/g, '')}</date>\n` : '';
    epg += programme.countries && programme.countries.length > 0 ? `${_tab(4)}<country lang="${lang}">${getCountry(programme.countries[0], 'iso_3166_1')}</country>\n` : '';
    if (programme.genres) {
        for(let genre of programme.genres) {
            epg += `${_tab(4)}<category lang="${lang}">${genre}</category>\n`;
        }
    }
    epg += programme.image ? `${_tab(4)}<icon src="${programme.image}"/>\n` : '';
    if (programme.media.video.length > 0) {
        epg += `${_tab(4)}<video>\n`;
        epg += `${_tab(6)}<aspect>${programme.media.video[0].display_aspect_ratio}</aspect>\n`;
        epg += `${_tab(6)}<colour>${programme.media.video[0].pix_fmt === 'gray' ? 'No' : 'Yes'}</colour>\n`;
        epg += `${_tab(6)}<quality>${programme.media.video[0].width * programme.media.video[0].height > 1382400 ? 'HDTV' : 'SDTV'}</quality>\n`;
        epg += `${_tab(4)}</video>\n`;
    }
    if (programme.media.audio.length > 0) {
        epg += `${_tab(4)}<audio>\n`;
        epg += `${_tab(6)}<stereo>${programme.media.audio[0].channels < 2 ? 'mono' : (programme.media.audio[0].channels > 2 ? 'dolby digital' : 'stereo')}</stereo>\n`;
        epg += `${_tab(4)}</audio>\n`;
    }
    //<previously-shown start="20080306000000" />
    if (programme.media.subtitle.length > 0) {
        epg += `${_tab(4)}<subtitles type="teletext" />\n`;
    }
    if (programme.certification) {
        if (process.env.EPG_CERTIFICATION) {
            epg += `${_tab(4)}<rating system="${process.env.EPG_CERTIFICATION}">\n`;
        }
        else {
            epg += `${_tab(4)}<rating>\n`;
        }
        epg += `${_tab(6)}<value>${programme.certification}</value>\n`;
        epg += `${_tab(4)}</rating>\n`;
    }
    if (programme.rating) {
        epg += `${_tab(4)}<star-rating>\n`;
        epg += `${_tab(6)}<value>${Math.floor(programme.rating*5)}/5</value>\n`;
        epg += `${_tab(4)}</star-rating>\n`;
    }
    return epg;    
}

function getMovieDefault(movie) {
    return getProgrammeDefault(movie);
}

function getShowDefault(show) {
    let epg = getProgramme(show);
    if (show.episode) {
        //<episode-num system="dd_progid">EP00847333.0303</episode-num>
        if (show.season) {
            epg += `${_tab(4)}<episode-num system="onscreen">S${show.season} E${show.episode}</episode-num>\n`;
        }
        else {
            epg += `${_tab(4)}<episode-num system="onscreen">${show.episode}</episode-num>\n`;
        }
    }
    return epg;
}

// KODI
function getProgrammeKodi(programme) {
    const lang = process.env.EPG_LANGUAGE;
    let epg = '';
    //TITLE
    epg += `${_tab(4)}<title lang="${lang}">${programme.title}</title>\n`;
    //SUB-TITLE
    epg += `${_tab(4)}<sub-title lang="${lang}">`;
    epg += `[COLOR khaki]${programme.subtitle && programme.subtitle + ' ' || ''}[/COLOR]`;
    epg += `[COLOR darkgray]${programme.countries && formatCountries(programme.countries)+' ' || ''}`;
    epg += `${programme.airDate && '('+programme.airDate.getUTCFullYear()+')' || ''}[/COLOR]`;
    epg += `</sub-title>\n`;
    //DESC
    epg += `${_tab(4)}<desc lang="${lang}">${programme.description || ''}`;    
    if (programme.credits) {
        epg += `[CR][CR]`;
        const directors = programme.credits.crew && programme.credits.crew.filter(c => c.job === 'Director');
        if (directors && directors.length > 0) {
            epg += `[COLOR gold][B]${_epgTranslation('Director')}[/B][/COLOR][CR]`;
            epg += `${directors[0].name}[CR][CR]`;
        }
        if (programme.credits.guest) {
            epg += `[COLOR gold][B]${_epgTranslation('Guests')}[/B][/COLOR][CR]`;
            for(let i=0; i < Math.min(4, programme.credits.guest.length); i++) {
                epg += `${programme.credits.guest[i].name}[CR]`;
            }
            epg += '[CR]';
        }
        if (programme.credits.cast) {
            epg += `[COLOR gold][B]${_epgTranslation('Casting')}[/B][/COLOR][CR]`;
            for(let i=0; i < Math.min(4, programme.credits.cast.length); i++) {
                epg += `${programme.credits.cast[i].name}[CR]`;
            }
            epg += '[CR]';
        }
    }
    epg += `</desc>\n`;
    //GENRES
    if (programme.genres) {
        let genres = '';
        for(let genre of programme.genres) {
            genres += genre+'/';
        }
        genres = _getKodiGenre(genres.substr(0, genres.length-1), lang, true);
        epg += `${_tab(4)}<category>${genres}</category>\n`;
    }
    epg += programme.image ? `${_tab(4)}<icon src="${programme.image}"/>\n` : '';
    return epg;
}

function getMovieKodi(movie) {
    const lang = process.env.EPG_LANGUAGE;
    let epg = '';
    //TITLE
    epg += `${_tab(4)}<title lang="${lang}">${movie.title}</title>\n`;
    //SUB-TITLE
    epg += `${_tab(4)}<sub-title lang="${lang}">`;
    epg += `[COLOR khaki]${movie.subtitle && movie.subtitle + ' ' || ''}[/COLOR]`;
    epg += `[COLOR darkgray]${movie.countries && formatCountries(movie.countries)+' ' || ''}`;
    epg += `${movie.airDate && '('+movie.airDate.getUTCFullYear()+')' || ''}[/COLOR]`;
    epg += `</sub-title>\n`;
    //DESC
    epg += `${_tab(4)}<desc lang="${lang}">${movie.description || ''}`;    
    if (movie.credits) {
        epg += `[CR][CR]`;
        const directors = movie.credits.crew && movie.credits.crew.filter(c => c.job === 'Director');
        if (directors && directors.length > 0) {
            epg += `[COLOR gold][B]${_epgTranslation('Director')}[/B][/COLOR][CR]`;
            epg += `${directors[0].name}[CR][CR]`;
        }
        const writers = movie.credits.crew && movie.credits.crew.filter(c => c.job === 'Writer');
        if (writers && writers.length > 0) {
            epg += `[COLOR gold][B]${_epgTranslation('Writers')}[/B][/COLOR][CR]`;
            for(let i=0; i < Math.min(2, writers.length); i++) {
                epg += `${writers[i].name}[CR]`;
            }
            epg += '[CR]';
        }
        if (movie.credits.cast) {
            epg += `[COLOR gold][B]${_epgTranslation('Casting')}[/B][/COLOR][CR]`;
            for(let i=0; i < Math.min(4, movie.credits.cast.length); i++) {
                epg += `${movie.credits.cast[i].name}[CR]`;
            }
            epg += '[CR]';
        }
    }
    epg += `</desc>\n`;
    //GENRES
    if (movie.genres) {
        let genres = '';
        for(let genre of movie.genres) {
            genres += genre+'/';
        }
        genres = _getKodiGenre(genres.substr(0, genres.length-1), lang, true);
        epg += `${_tab(4)}<category>${genres}</category>\n`;
    }
    epg += movie.image ? `${_tab(4)}<icon src="${movie.image}"/>\n` : '';
    return epg;
}

function getShowKodi(show) {
    const lang = process.env.EPG_LANGUAGE;
    let epg = '';
    //TITLE
    epg += `${_tab(4)}<title lang="${lang}">${show.title}</title>\n`;
    //SUB-TITLE
    epg += `${_tab(4)}<sub-title lang="${lang}">`;
    epg += `[COLOR dodgerblue]${show.season && 'S'+show.season+' ' || ''}[/COLOR]`;
    epg += `[COLOR deepskyblue]${show.episode && 'E'+show.episode+' ' || ''}[/COLOR]`;
    epg += `[COLOR khaki]${show.subtitle && show.subtitle + ' ' || ''}[/COLOR]`;
    epg += `[COLOR darkgray]${show.countries && formatCountries(show.countries)+' ' || ''}`;
    epg += `${show.airDate && '('+show.airDate.getUTCFullYear()+')' || ''}[/COLOR]`;
    epg += `</sub-title>\n`;
    //DESC
    epg += `${_tab(4)}<desc lang="${lang}">${show.description || ''}`;    
    if (show.credits) {
        epg += `[CR][CR]`;
        const directors = show.credits.crew && show.credits.crew.filter(c => c.job === 'Director');
        if (directors && directors.length > 0) {
            epg += `[COLOR gold][B]${_epgTranslation('Director')}[/B][/COLOR][CR]`;
            epg += `${directors[0].name}[CR][CR]`;
        }
        const writers = movie.credits.crew && movie.credits.crew.filter(c => c.job === 'Writer');
        if (writers && writers.length > 0) {
            epg += `[COLOR gold][B]${_epgTranslation('Writers')}[/B][/COLOR][CR]`;
            for(let i=0; i < Math.min(2, writers.length); i++) {
                epg += `${writers[i].name}[CR]`;
            }
            epg += '[CR]';
        }
        if (show.credits.guest) {
            epg += `[COLOR gold][B]${_epgTranslation('Guests')}[/B][/COLOR][CR]`;
            for(let i=0; i < Math.min(4, show.credits.guest.length); i++) {
                epg += `${show.credits.guest[i].name}[CR]`;
            }
            epg += '[CR]';
        }
        if (show.credits.cast) {
            epg += `[COLOR gold][B]${_epgTranslation('Casting')}[/B][/COLOR][CR]`;
            for(let i=0; i < Math.min(4, show.credits.cast.length); i++) {
                epg += `${show.credits.cast[i].name}[CR]`;
            }
            epg += '[CR]';
        }
    }
    epg += `</desc>\n`;
    //GENRES
    if (show.genres) {
        let genres = '';
        for(let genre of show.genres) {
            genres += genre+'/';
        }
        genres = _getKodiGenre(genres.substr(0, genres.length-1), lang, true);
        epg += `${_tab(4)}<category>${genres}</category>\n`;
    }
    epg += show.image ? `${_tab(4)}<icon src="${show.image}"/>\n` : '';
    return epg;
}

function getProgramme(programme) {
    return process.env.EPG_KODI ? getProgrammeKodi(programme) : getProgrammeDefault(programme);
}

function getMovie(movie) {
    return process.env.EPG_KODI ? getMovieKodi(movie) : getMovieDefault(movie);
}

function getShow(show) {
    return process.env.EPG_KODI ? getShowKodi(show) : getShowDefault(show);
}

module.exports = {
    getProgramme,
    getMovie,
    getShow
}