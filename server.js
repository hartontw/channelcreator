require('./lib/globals');
const express = require('express');
const app = express();
const path = require('path');

function startServer()
{
    app.use(express.static(path.join(__dirname, process.env.PATH_CHANNELS)));
    app.listen(process.env.PORT, process.env.IP);
    console.log(`channelCreator listening on http://${process.env.IP}:${process.env.PORT}`);
}

const folder = 'C:/Users/Harton/Videos/Test/';
/*
app.use(express.static(folder));
app.listen(process.env.PORT, process.env.IP);
console.log(`channelCreator listening on http://${process.env.IP}:${process.env.PORT}`);
*/

const {Programme, Movie, Show} = require('./lib/programme.js')

Programme.Create(folder+'Brave.mkv', '14:30')
.then(auto => {
    console.log(auto);
})
.catch(console.error);
