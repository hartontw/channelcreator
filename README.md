# Channel Creator

Tool for create TV channels from online media

config/environment.json
```json
{
    "ip": "127.0.0.1",
    "port": "8080",
    "content_path": "./content",
    "epg": {
        "domain": "channelCreator",
        "language": "en",
        "certification": "US",
        "kodi": true
    },
    "moviedb": {
        "api_key": "<https://www.themoviedb.org/settings/api>",
        "language": "en-US",
        "region": "US",
        "certification_country": "US"
    },
    "youtube": {
        "api_key": "<https://console.developers.google.com/apis/dashboard>"
    },
    "transmission": {
        "ip": "127.0.0.1",
        "port": "9091"
    }
}
```

environment vars
```js
process.env.IP 
process.env.PORT 
process.env.CONTENT_PATH 
process.env.EPG_DOMAIN 
process.env.EPG_LANGUAGE 
process.env.EPG_CERTIFICATION 
process.env.EPG_KODI 
process.env.MOVIEDB_API_KEY 
process.env.MOVIEDB_LANGUAGE 
process.env.MOVIEDB_REGION 
process.env.MOVIEDB_CERTIFICATION_COUNTRY 
process.env.YOUTUBE_API_KEY 
process.env.TRANSMISSION_IP 
process.env.TRANSMISSION_PORT 
```